package arquivoNotaFiscalTest

import (
	"fmt"
	"layout-most/arquivoNotaFiscal"
	"testing"
	"time"
)

func TestHeaderRetorno (t *testing.T) {

	data := time.Date(2019, 10,1,1,5,20,0, time.UTC)

	cabecalho := arquivoNotaFiscal.GetHeaderNota(data,"2222222222","3333333333",22, 33, "666666666666666", "55555555555555555555", 333, 1 )

	if cabecalho != "101102019222222222233333333332233666666666666666555555555555555555553331" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")
	}
}

func TestDetalheRetorno (t *testing.T) {

	valor5:= float32(777.77)
	valor8:= float32(888888.88)

	detalhe := arquivoNotaFiscal.GetDetalheNota("4444444444",22, 5555555555555, 666666, valor8,valor8,valor5,valor8,valor8,valor8,valor8)

	if detalhe != "2444444444422555555555555566666688888888888888887777788888888888888888888888888888888" {
		fmt.Println("detalhe", detalhe)
		t.Error("detalhe não é compativel")

	}

}

func TestTraillerNota (t *testing.T) {

	valor8:= float64(888888.88)

	Trailler := arquivoNotaFiscal.GetTraillerNota("4444444444",22, 55555, 6666666666, valor8, valor8, valor8)

	if Trailler != "3444444444422555556666666666888888888888888888888888" {
		fmt.Println("Trailler", Trailler)
		t.Error("Trailler não é compativel")

	}

}