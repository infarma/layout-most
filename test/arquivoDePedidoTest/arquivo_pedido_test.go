package arquivoDePedidoTest

import (
	"layout-most/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {

	f, err := os.Open("../fileTest/mock.txt")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var header arquivoDePedido.Header
	//1222222222222222333333333333333333331011201923222222222222222444999
	header.TipoRegistro = 1
	header.CodigoCliente = "222222222222222"
	header.NumeroPedido = "33333333333333333333"
	header.DataPedido = 10112019
	header.TipoCompra = "2"
	header.TipoRetorno = "3"
	header.CnpjOLFaturamento = "222222222222222"
	header.ApontadorCondicaoComercial = 44444
	header.PrazoApontadorComercial = "999"


	if header.TipoRegistro != arq.Header.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if header.CodigoCliente != arq.Header.CodigoCliente {
		t.Error("CodigoCliente não é compativel")
	}
	if header.NumeroPedido != arq.Header.NumeroPedido {
		t.Error("NumeroPedido não é compativel")
	}
	if header.DataPedido != arq.Header.DataPedido {
		t.Error("DataPedido não é compativel")
	}
	if header.TipoCompra != arq.Header.TipoCompra {
		t.Error("TipoCompra não é compativel")
	}
	if header.TipoRetorno != arq.Header.TipoRetorno {
		t.Error("TipoRetorno não é compativel")
	}
	if header.CnpjOLFaturamento != arq.Header.CnpjOLFaturamento {
		t.Error("CnpjOLFaturamento não é compativel")
	}
	if header.ApontadorCondicaoComercial != arq.Header.ApontadorCondicaoComercial {
		t.Error("ApontadorCondicaoComercial não é compativel")
	}
	if header.PrazoApontadorComercial != arq.Header.PrazoApontadorComercial {
		t.Error("PrazoApontadorComercial não é compativel")
	}


	var detalhe arquivoDePedido.Detalhe
	//233333333333333333333444444444444455555666669999988823
	detalhe.TipoRegistro = 2
	detalhe.NumeroPedido = "33333333333333333333"
	detalhe.CodigoProduto = 4444444444444
	detalhe.Quantidade = 55555
	detalhe.ApontadorCondicaoComercial = 66666
	detalhe.Desconto = 99999
	detalhe.Prazo = 888
	detalhe.UtilizacaoDesconto = "2"
	detalhe.UtilizacaoPrazo = "3"

	if detalhe.TipoRegistro != arq.Detalhe[0].TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if detalhe.NumeroPedido != arq.Detalhe[0].NumeroPedido {
		t.Error("NumeroPedido não é compativel")
	}
	if detalhe.CodigoProduto != arq.Detalhe[0].CodigoProduto {
		t.Error("CodigoProduto não é compativel")
	}
	if detalhe.Quantidade != arq.Detalhe[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}
	if detalhe.ApontadorCondicaoComercial != arq.Detalhe[0].ApontadorCondicaoComercial {
		t.Error("ApontadorCondicaoComercial não é compativel")
	}
	if detalhe.Desconto != arq.Detalhe[0].Desconto {
		t.Error("Desconto não é compativel")
	}
	if detalhe.Prazo != arq.Detalhe[0].Prazo {
		t.Error("Prazo não é compativel")
	}
	if detalhe.UtilizacaoDesconto != arq.Detalhe[0].UtilizacaoDesconto {
		t.Error("UtilizacaoDesconto não é compativel")
	}
	if detalhe.UtilizacaoPrazo != arq.Detalhe[0].UtilizacaoPrazo {
		t.Error("UtilizacaoPrazo não é compativel")
	}

	var trailer arquivoDePedido.Trailler
	//344444444444444444444555559999999999
	trailer.TipoRegistro = 3
	trailer.NumeroPedido = "44444444444444444444"
	trailer.QuantidadeItens = 55555
	trailer.QuantidadeUnidades = 9999999999

	if trailer.TipoRegistro != arq.Trailler.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if trailer.NumeroPedido != arq.Trailler.NumeroPedido {
		t.Error("NumeroPedido não é compativel")
	}
	if trailer.QuantidadeItens != arq.Trailler.QuantidadeItens {
		t.Error("QuantidadeItens não é compativel")
	}
	if trailer.QuantidadeUnidades != arq.Trailler.QuantidadeUnidades {
		t.Error("UtilizacaoPrazo não é compativel")
	}

}