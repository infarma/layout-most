package retornoDePedido

import (
	"fmt"
	"layout-most/retornoDePedido"
	"testing"
	"time"
)

func TestHeaderRetorno (t *testing.T) {

	data := time.Date(2019, 10,1,1,5,20,0, time.UTC)

	cabecalho := retornoDePedido.GetHeaderRetorno("222222222222222", "33333333333333333333",data,data,666666666666,"888888888888888" )

	if cabecalho != "122222222222222233333333333333333333011020190105200666666666666888888888888888" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")

	}
}

func TestProdutoRetorno (t *testing.T) {

	desconto:= float32(777.77)

	detalhe := retornoDePedido.GetDetalheRetorno(3333333333333,"44444444444444444444", "1", 55555, desconto, 333,44444,"1")

	if detalhe != "2333333333333344444444444444444444155555777773334444400000000000000000000000000000000000000000000000001" {
		fmt.Println("detalhe", detalhe)
		t.Error("detalhe não é compativel")

	}

}

func TestTraillerRetorno (t *testing.T) {

	limite:= float64(888888888888.77)

	Trailler := retornoDePedido.GetTraillerRetorno("44444444444444444444",55555, 66666, 77777, limite)

	if Trailler != "34444444444444444444455555666667777788888888888877" {
		fmt.Println("Trailler", Trailler)
		t.Error("Trailler não é compativel")

	}

}