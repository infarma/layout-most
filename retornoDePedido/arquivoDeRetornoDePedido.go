package retornoDePedido

import (
	"fmt"
	"strings"
	"time"
)

func GetHeaderRetorno(	CnpjFarmacia string,  NumeroPedidoIndustria string, DataProcessamento time.Time, HoraProcessamento time.Time, NumeroPedidoOL int64, CnpjDistribuidorFaturamento string ) string{


	cabecalho := "1"
	cabecalho += fmt.Sprintf("%015s", CnpjFarmacia)
	cabecalho += fmt.Sprintf("%020s", NumeroPedidoIndustria)
	cabecalho += DataProcessamento.Format("02012006")
	cabecalho += HoraProcessamento.Format("1504050")
	cabecalho += fmt.Sprintf("%012d", NumeroPedidoOL )
	cabecalho += fmt.Sprintf("%015s", CnpjDistribuidorFaturamento )

	return cabecalho
}

func GetDetalheRetorno(	CodigoProduto int64,  NumeroPedido string, CondicaoPagamento string, QuantidadeAtendida int32, DescontoAplicado float32, PrazoConcedido int32, QuantidadeNaoAtendida int32, Motivo string ) string{

	detalhe := "2"
	detalhe += fmt.Sprintf("%013d", CodigoProduto)
	detalhe += fmt.Sprintf("%020s", NumeroPedido)
	detalhe += fmt.Sprintf("%01s", CondicaoPagamento)
	detalhe += fmt.Sprintf("%05d", QuantidadeAtendida)
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", DescontoAplicado), ".", "", 1))
	detalhe += fmt.Sprintf("%03d", PrazoConcedido )
	detalhe += fmt.Sprintf("%05d", QuantidadeNaoAtendida )
	detalhe += fmt.Sprintf("%050s", Motivo )

	return detalhe
}

func GetTraillerRetorno(	NumeroPedido string,  QuantidadeLinhas int32, QuantidadeItensAtendidos int32, QuantidaeItensNaoAtendida int32, LimiteDisponivel float64 ) string{

	detalhe := "3"
	detalhe += fmt.Sprintf("%020s", NumeroPedido)
	detalhe += fmt.Sprintf("%05d", QuantidadeLinhas)
	detalhe += fmt.Sprintf("%05d", QuantidadeItensAtendidos)
	detalhe += fmt.Sprintf("%05d", QuantidaeItensNaoAtendida)
	detalhe += fmt.Sprintf("%014s", strings.Replace(fmt.Sprintf("%.2f", LimiteDisponivel), ".", "", 1))

	return detalhe
}