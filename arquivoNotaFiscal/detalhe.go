package arquivoNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Detalhe struct {
	TipoRegistro               int32   `json:"TipoRegistro"`
	NumeroPedidoIndustria      string  `json:"NumeroPedidoIndustria"`
	NumeroParcela              int32   `json:"NumeroParcela"`
	CodigoEanProduto           int64   `json:"CodigoEanProduto"`
	QuantidadeFaturada         int32   `json:"QuantidadeFaturada"`
	ValorBruto                 float32 `json:"ValorBruto"`
	ValorLiquido               float32 `json:"ValorLiquido"`
	PercentualDesconto         float32 `json:"PercentualDesconto"`
	ValorDesconto              float32 `json:"ValorDesconto"`
	ValorRepasse               float32 `json:"ValorRepasse"`
	ValorICMS                  float32 `json:"ValorICMS"`
	ValorSbstituicaoTributaria float32 `json:"ValorSbstituicaoTributaria"`
}

func (d *Detalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDetalhe

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedidoIndustria, "NumeroPedidoIndustria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroParcela, "NumeroParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoEanProduto, "CodigoEanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.QuantidadeFaturada, "QuantidadeFaturada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorBruto, "ValorBruto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorLiquido, "ValorLiquido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDesconto, "PercentualDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDesconto, "ValorDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorRepasse, "ValorRepasse")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorICMS, "ValorICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorSbstituicaoTributaria, "ValorSbstituicaoTributaria")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":               {0, 1, 0},
	"NumeroPedidoIndustria":      {1, 11, 0},
	"NumeroParcela":              {11, 13, 0},
	"CodigoEanProduto":           {13, 26, 0},
	"QuantidadeFaturada":         {26, 32, 0},
	"ValorBruto":                 {32, 40, 2},
	"ValorLiquido":               {40, 48, 2},
	"PercentualDesconto":         {48, 53, 2},
	"ValorDesconto":              {53, 61, 2},
	"ValorRepasse":               {61, 69, 2},
	"ValorICMS":                  {69, 77, 2},
	"ValorSbstituicaoTributaria": {77, 85, 2},
}
