package arquivoNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	TipoRegistro                  int32  `json:"TipoRegistro"`
	DataEmissao                   int32  `json:"DataEmissao"`
	NumeroPedidoIndustria         string `json:"NumeroPedidoIndustria"`
	NumeroPedidoCliente           string `json:"NumeroPedidoCliente"`
	NumeroParcela                 int32  `json:"NumeroParcela"`
	NumeroTotalParcelasNotaFiscal int32  `json:"NumeroTotalParcelasNotaFiscal"`
	CodigoCliente                 string `json:"CodigoCliente"`
	NumeroNotaFiscal              string `json:"NumeroNotaFiscal"`
	SerieNotaFiscal               int32  `json:"SerieNotaFiscal"`
	NaturezaOperacao              int32  `json:"NaturezaOperacao"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataEmissao, "DataEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoIndustria, "NumeroPedidoIndustria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroParcela, "NumeroParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroTotalParcelasNotaFiscal, "NumeroTotalParcelasNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroNotaFiscal, "NumeroNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.SerieNotaFiscal, "SerieNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NaturezaOperacao, "NaturezaOperacao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                  {0, 1, 0},
	"DataEmissao":                   {1, 9, 0},
	"NumeroPedidoIndustria":         {9, 19, 0},
	"NumeroPedidoCliente":           {19, 29, 0},
	"NumeroParcela":                 {29, 31, 0},
	"NumeroTotalParcelasNotaFiscal": {31, 33, 0},
	"CodigoCliente":                 {33, 48, 0},
	"NumeroNotaFiscal":              {48, 68, 0},
	"SerieNotaFiscal":               {68, 71, 0},
	"NaturezaOperacao":              {71, 72, 0},
}
