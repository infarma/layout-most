package arquivoNotaFiscal

import (
	"fmt"
	"strings"
	"time"
)

func GetHeaderNota(	DataEmissao time.Time,  NumeroPedidoIndustria string, NumeroPedidoCliente string, NumeroParcela int32, NumeroTotalParcelasNotaFiscal int32, CodigoCliente string, NumeroNotaFiscal string, SerieNotaFiscal int32, NaturezaOperacao int32   ) string{

	cabecalho := "1"
	cabecalho += DataEmissao.Format("02012006")
	cabecalho += fmt.Sprintf("%010s", NumeroPedidoIndustria)
	cabecalho += fmt.Sprintf("%010s", NumeroPedidoCliente )
	cabecalho += fmt.Sprintf("%02d", NumeroParcela )
	cabecalho += fmt.Sprintf("%02d", NumeroTotalParcelasNotaFiscal )
	cabecalho += fmt.Sprintf("%015s", CodigoCliente )
	cabecalho += fmt.Sprintf("%020s", NumeroNotaFiscal )
	cabecalho += fmt.Sprintf("%03d", SerieNotaFiscal )
	cabecalho += fmt.Sprintf("%01d", NaturezaOperacao )

	return cabecalho
}

func GetDetalheNota( NumeroPedidoIndustria string,  NumeroParcela int32, CodigoEanProduto int64, QuantidadeFaturada int32, ValorBruto float32, ValorLiquido float32, PercentualDesconto float32, ValorDesconto float32, ValorRepasse float32, ValorICMS float32, ValorSbstituicaoTributaria float32 ) string{

	detalhe := "2"
	detalhe += fmt.Sprintf("%010s", NumeroPedidoIndustria)
	detalhe += fmt.Sprintf("%02d", NumeroParcela)
	detalhe += fmt.Sprintf("%013d", CodigoEanProduto)
	detalhe += fmt.Sprintf("%06d", QuantidadeFaturada)
	detalhe += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorBruto), ".", "", 1))
	detalhe += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorLiquido), ".", "", 1))
	detalhe += fmt.Sprintf("%05s", strings.Replace(fmt.Sprintf("%.2f", PercentualDesconto), ".", "", 1))
	detalhe += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorDesconto), ".", "", 1))
	detalhe += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorRepasse), ".", "", 1))
	detalhe += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorICMS), ".", "", 1))
	detalhe += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorSbstituicaoTributaria), ".", "", 1))

	return detalhe

}

func GetTraillerNota(	NumeroPedidoIndustria string,  NumeroParcela int32, QuantidadeItens int32, QuantidadeProdutosFaturadas int64, ValorBruto float64, ValorLiquido float64, ValorTotalICMS float64 ) string{

	Trailler := "3"
	Trailler += fmt.Sprintf("%010s", NumeroPedidoIndustria)
	Trailler += fmt.Sprintf("%02d", NumeroParcela)
	Trailler += fmt.Sprintf("%05d", QuantidadeItens)
	Trailler += fmt.Sprintf("%010d", QuantidadeProdutosFaturadas)
	Trailler += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorBruto), ".", "", 1))
	Trailler += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorLiquido), ".", "", 1))
	Trailler += fmt.Sprintf("%08s", strings.Replace(fmt.Sprintf("%.2f", ValorTotalICMS), ".", "", 1))

	return Trailler
}