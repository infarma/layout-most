package arquivoNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailler struct {
	TipoRegistro                int32   `json:"TipoRegistro"`
	NumeroPedidoIndustria       string  `json:"NumeroPedidoIndustria"`
	NumeroParcela               int32   `json:"NumeroParcela"`
	QuantidadeItens             int32   `json:"QuantidadeItens"`
	QuantidadeProdutosFaturadas int64   `json:"QuantidadeProdutosFaturadas"`
	ValorBruto                  float32 `json:"ValorBruto"`
	ValorLiquido                float32 `json:"ValorLiquido"`
	ValorTotalICMS              float32 `json:"ValorTotalICMS"`
}

func (t *Trailler) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailler

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroPedidoIndustria, "NumeroPedidoIndustria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroParcela, "NumeroParcela")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeItens, "QuantidadeItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeProdutosFaturadas, "QuantidadeProdutosFaturadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorBruto, "ValorBruto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorLiquido, "ValorLiquido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorTotalICMS, "ValorTotalICMS")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailler = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                {0, 1, 0},
	"NumeroPedidoIndustria":       {1, 11, 0},
	"NumeroParcela":               {11, 13, 0},
	"QuantidadeItens":             {13, 18, 0},
	"QuantidadeProdutosFaturadas": {18, 28, 0},
	"ValorBruto":                  {28, 36, 2},
	"ValorLiquido":                {36, 44, 2},
	"ValorTotalICMS":              {44, 52, 2},
}
