package layout_most

import (
	"bitbucket.org/infarma/layout-most/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}
