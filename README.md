## Arquivo de Pedido
gerador-layouts arquivoDePedido header TipoRegistro:int32:0:1 CodigoCliente:string:1:16 NumeroPedido:string:16:36 DataPedido:int32:36:44 TipoCompra:string:44:45 TipoRetorno:string:45:46 CnpjOLFaturamento:string:46:61 ApontadorCondicaoComercial:int32:61:66 PrazoApontadorComercial:string:66:81

gerador-layouts arquivoDePedido detalhe TipoRegistro:int32:0:1 NumeroPedido:string:1:21 CodigoProduto:int64:21:34 Quantidade:int32:34:39 ApontadorCondicaoComercial:int32:39:44 Desconto:int32:44:49 Prazo:int32:49:52 UtilizacaoDesconto:string:52:53 UtilizacaoPrazo:string:53:54

gerador-layouts arquivoDePedido trailler TipoRegistro:int32:0:1 NumeroPedido:string:1:21 QuantidadeItens:int32:21:26 QuantidadeUnidades:int64:26:36


## Arquivo de Retorno
gerador-layouts retornoDePedido header TipoRegistro:int32:0:1 CnpjFarmacia:string:1:16 NumeroPedidoIndustria:string:16:36 DataProcessamento:int32:36:44 HoraProcessamento:int32:44:51 NumeroPedidoOL:int64:51:63 CnpjDistribuidorFaturamento:string:63:78

gerador-layouts retornoDePedido detalhe TipoRegistro:int32:0:1 CodigoProduto:int64:1:14 NumeroPedido:string:14:34 CondicaoPagamento:string:34:35 QuantidadeAtendida:int32:35:40 DescontoAplicado:float64:40:45:2 PrazoConcedido:int32:45:48 QuantidadeNaoAtendida:int32:48:53 Motivo:string:53:103

gerador-layouts retornoDePedido trailler TipoRegistro:int32:0:1 NumeroPedido:string:1:21 QuantidadeLinhas:int32:21:26 QuantidadeItensAtendidos:int32:26:31 QuantidaeItensNaoAtendida:int32:31:36 LimiteDisponivel:float64:36:50:2


## Arquivo de Nota Fiscal
gerador-layouts arquivoNotaFiscal header TipoRegistro:int32:0:1 DataEmissao:int32:1:9 NumeroPedidoIndustria:string:9:19 NumeroPedidoCliente:string:19:29 NumeroParcela:int32:29:31 NumeroTotalParcelasNotaFiscal:int32:31:33 CodigoCliente:string:33:48 NumeroNotaFiscal:string:48:68 SerieNotaFiscal:int32:68:71 NaturezaOperacao:int32:71:72

gerador-layouts arquivoNotaFiscal detalhe TipoRegistro:int32:0:1 NumeroPedidoIndustria:string:1:11 NumeroParcela:int32:11:13 CodigoEanProduto:int64:13:26 QuantidadeFaturada:int32:26:32 ValorBruto:float32:32:40:2 ValorLiquido:float32:40:48:2 PercentualDesconto:float32:48:53:2 ValorDesconto:float32:53:61:2 ValorRepasse:float32:61:69:2 ValorICMS:float32:69:77:2 ValorSbstituicaoTributaria:float32:77:85:2

gerador-layouts arquivoNotaFiscal trailler TipoRegistro:int32:0:1 NumeroPedidoIndustria:string:1:11 NumeroParcela:int32:11:13 QuantidadeItens:int32:13:18 QuantidadeProdutosFaturadas:int64:18:28 ValorBruto:float32:28:36:2 ValorLiquido:float32:36:44:2 ValorTotalICMS:float32:44:52:2